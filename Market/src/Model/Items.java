package Model;
/**
 * @author Diego Fernandes Malafaia
 * date:12/06/16
 */

public class Items {
	
	public int quantityA = 0;
	public int totalOrderA = 0;
	public int dicountA = 0;
	public int quantityB = 0;
	public int totalOrderB = 0;
	public int dicountB = 0;
	public int quantityC = 0;
	public int totalOrderC = 0;
	public int quantityD = 0;
	public int totalOrderD = 0;
	public int finalOrder = 0;	
	

	public int getQuantityA() {
		return quantityA;
	}

	public void setQuantityA(int quantityA) {
		this.quantityA = quantityA;
	}

	public int getTotalOrderA() {
		return totalOrderA;
	}

	public void setTotalOrderA(int totalOrderA) {
		this.totalOrderA = totalOrderA;
	}

	public int getDicountA() {
		return dicountA;
	}

	public void setDicountA(int dicountA) {
		this.dicountA = dicountA;
	}

	public int getQuantityB() {
		return quantityB;
	}

	public void setQuantityB(int quantityB) {
		this.quantityB = quantityB;
	}

	public int getTotalOrderB() {
		return totalOrderB;
	}

	public void setTotalOrderB(int totalOrderB) {
		this.totalOrderB = totalOrderB;
	}

	public int getDicountB() {
		return dicountB;
	}

	public void setDicountB(int dicountB) {
		this.dicountB = dicountB;
	}

	public int getC() {
		return quantityC;
	}

	public void setC(int c) {
		quantityC = c;
	}

	public int getD() {
		return quantityD;
	}

	public void setD(int d) {
		quantityD = d;
	}

	public int getfinalOrder() {
		return finalOrder;
	}

	public void setFinalOrder(int finalOrder) {
		this.finalOrder = finalOrder;
	}	
	
	public int getQuantityC() {
		return quantityC;
	}

	public void setQuantityC(int quantityC) {
		this.quantityC = quantityC;
	}

	public int getTotalOrderC() {
		return totalOrderC;
	}

	public void setTotalOrderC(int totalOrderC) {
		this.totalOrderC = totalOrderC;
	}

	public int getQuantityD() {
		return quantityD;
	}

	public void setQuantityD(int quantityD) {
		this.quantityD = quantityD;
	}

	public int getTotalOrderD() {
		return totalOrderD;
	}

	public void setTotalOrderD(int totalOrderD) {
		this.totalOrderD = totalOrderD;
	}


}