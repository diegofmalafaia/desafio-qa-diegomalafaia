package Control;
import View.ItemRules;

/**
 * @author Diego Fernandes Malafaia
 * date:12/06/16
 */
public class Sell {
	
	
	public static void main(String[] Args) {

		ItemRules rules = new ItemRules();

		rules.quantityB += 3;
		rules.quantityA += 7;
		rules.quantityA += 3;
		rules.quantityB += 6;
		rules.quantityC += 3;
		rules.quantityD += 5;

	
		rules.RuleForItemA();
		System.out.println( "-----------------------------------------------------------------------------------------------");
		rules.RuleForItemB();
		System.out.println( "-----------------------------------------------------------------------------------------------");
		rules.RuleForItemC();
		System.out.println( "-----------------------------------------------------------------------------------------------");
		rules.RuleForItemD();
		System.out.println( "-----------------------------------------------------------------------------------------------");			
		rules.TotalValueOfOrder();		
		
	}

}