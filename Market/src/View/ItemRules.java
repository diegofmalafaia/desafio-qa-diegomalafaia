package View;
import Model.Items;

/**
	 * @author Diego Malafaia
	 * date:12/06/16
	 */

public class ItemRules extends Items {

	/**@author Diego Malafaia
	 * This method calculates and shows the result for the item A
	 * Unit Price: 50
	 * Special Price: 3 for 130
	 * Date: 12/06/2016
	 */
	
	public void RuleForItemA() {

		int qntA = this.quantityA;
		System.out.println("Item A = " + qntA + " * 50/unit" );

		// Checking no items.
		if (qntA == 0) {
			System.out.println("There are no itens A");
			// Checking quantity without discount.
		} else if (qntA < 3) {
			this.setTotalOrderA(qntA * 50);
			System.out.println("Total Order for item A = " + this.getTotalOrderA());
			// Doing discount for items A
		} else if (qntA >= 3) {
			do {
				qntA = this.quantityA -= 3;
				this.setTotalOrderA(this.totalOrderA += 150);
				this.setDicountA(this.dicountA += 20);

			} while (qntA >= 3);
			// Calculating the itens without discount.
			this.setTotalOrderA(this.totalOrderA += qntA * 50);

			System.out.println("Special price discount for item A = " + this.getDicountA());
			System.out.println("Total Order for item A (without discount) = " + this.getTotalOrderA());
			System.out.println("Final Order for item A = " + (this.getTotalOrderA() - this.getDicountA()));
			this.setTotalOrderA(this.getTotalOrderA() - this.getDicountA());			
		}
	}

	/**@author Diego Malafaia
	 * This method calculates and shows the result for the item B
	 * Unit Price: 30
	 * Special Price : 2 for 45
	 * Date: 12/06/2016
	 */	
	public void RuleForItemB() {

		int qntB = this.quantityB;
		System.out.println("Item B = " + qntB + " * 30/unit" );
		// Checking no items.
		if (qntB == 0) {
			System.out.println("There are no itens B");
			// Checking quantity without discount.
		} else if (qntB < 2) {
			this.setTotalOrderB(qntB * 30);
			System.out.println("Total Order for item A = " + this.getTotalOrderB());
			// Doing discount for items A
		} else if (qntB >= 2) {
			do {
				qntB = this.quantityB -= 2;
				this.setTotalOrderB(this.totalOrderB += 60);
				this.setDicountB(this.dicountB += 15);

			} while (qntB >= 3);
			// Calculating the itens without discount.
			this.setTotalOrderB(this.totalOrderB += qntB * 30);

			System.out.println("Special price discount for item B = " + this.getDicountB());
			System.out.println("Total Order for item B (without discount) = " + this.getTotalOrderB());
			System.out.println("Final Order form  item B = " + (this.getTotalOrderB() - this.getDicountB()));
			this.setTotalOrderB(this.getTotalOrderB() - this.getDicountB());
		}

	}
	
	/**@author Diego Malafaia
	 * This method calculates and shows the result for the item C
	 * Unit Price: 20
	 * Date: 12/06/2016
	 */	
	public void RuleForItemC() {

		int qntC = this.quantityC;
		int totalOrderC = 0;
		System.out.println("Item C = " + qntC + " * 20/unit" );
		
		// Checking the presence of no itens.
		if (qntC == 0) {
			System.out.println("There are no itens C");
		}
		// Calculating the itens C.
		else {			
			totalOrderC = qntC * 20;
			setTotalOrderC(totalOrderC);
			System.out.println("Total Order for item C = " + this.getTotalOrderC());
		}

	}
	
	/**@author Diego Malafaia
	 * This method calculates and shows the result for the item D
	 * Unit Price: 15
	 * Date: 12/06/2016
	 */	
	public void RuleForItemD() {
		int qntD = getQuantityD();
		int totalOrderD = 0;
		System.out.println("Item C = " + qntD + " * 15/unit" );

		// Checking the presence of no itens.
		if (qntD == 0) {
			System.out.println("There are no itens D");
		}
		else {			
			totalOrderD = qntD * 15;
			setTotalOrderD(totalOrderD);
			System.out.println("Final order for item D = " + getTotalOrderD());
		}
	}
	
	
	public void TotalValueOfOrder(){
	
	int total = (this.getTotalOrderA() + this.getTotalOrderB() + this.getTotalOrderC() + this.getTotalOrderD());
	System.out.println("Final order value = " + total);
		
	}

}