# language: pt

Funcionalidade: Testar funcionalidade de alterar o tamanho da fonte, onde o usuario consegue definir o tamanho de fonte
que mais lhe agrada. O sistema deve fornecer tres tipos de fonte (Pequena, Media, Grande).
Obs: assim que o tamanho da fonte eh alterado, todas as conversas devem ser exibidas com o tamanho escolhido pelo usuario.

Esquema do Cenario:  Testar funcionalidade de alterar o tamanho da fonte.	
Dado o usuario inicia o app
Quando atraves do display do smartphone o usuario toca no menu de opcoes (tres pontinhos).
Entao as opcoes serao exibidas e dentre elas a opcao de 'configuracoes'.
Quando o usuario seleciona a opcao de configuracoes.
Entao o menu de configuracoes eh exibido contendo a opcao 'Conversas'.
Quando o usuario seleciona o menu 'Conversas'.
Entao o menu de 'Conversas' eh exibido contendo a occao 'Tamanho da Fonte'.
Quando o usuario seleciona a opcao 'Tamanho da Fonte'.
Entao uma nova janela eh exibida ao usuario.
E as opcoes disponiveis sao (Pequena, Media e Grande).
Quando o usuario seleciona um tamanho de fonte.
Entao o sistema deve alterar o tamanho de fonte de todas as conversas para o 'Tamanho da Fonte' selecionado.