# language: pt

Funcionalidade: Testar funcionalidade de backup de mensagens, onde o usuario consegue salvar no servidor do whatsapp
seus registros de conversas. O sistema deve prover a funcionalidade de forma facil e com seguranca dos dados.

Esquema do Cenario:  Testar funcionalidade de backup de mensagens.	
Dado o usuario inicia o app
Quando atraves do display do smartphone o usuario toca no menu de opcoes (tres pontinhos).
Entao as opcoes serao exibidas e dentre elas a opcao de 'configuracoes'.
Quando o usuario seleciona a opcao de configuracoes.
Entao o menu de configuracoes eh exibido contendo a opcao 'Conversas'.
Quando o usuario seleciona o menu 'Conversas'.
Entao o menu de 'Conversas' eh exibido contendo a opcao 'Backup de conversas'
Quando o usuario seleciona a opcao 'Backup de conversas'.
Entao o a pagina de 'Backup de conversas' eh exibida.
E deve conter os dados do ultimo backup.
E na mesma pagina conter tambem o botao 'FAZER BACKUP'
Quando O usuario atraves do display toca no botao 'FAZER BACKUP'
Entao uma nova janela eh exibida solicitando a confirmacao do backup
Quando o confirma o backup.
Entao o backup eh realizado com sucesso.